package main;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import capbility.Capability;
import steps.Login;
import steps.Repayment;

public class MainClass extends Capability
{
	public List<String[]> data=new ArrayList<String[]>();
	WebDriver driver;
	
	@BeforeTest
	public void openchrome()
	{
		driver = WebCapability();
		driver.manage().window().maximize();
		
	}
	@Test(priority = 1)
	public void login() throws InterruptedException
	{
		new Login(driver);
	}
	@Test(priority = 2)
	public void repayment()
	{
		new Repayment(driver);
	}
	@AfterTest
	public void CloseBrowser()
	{

		driver.quit	();
	}

	
	
	
	}
