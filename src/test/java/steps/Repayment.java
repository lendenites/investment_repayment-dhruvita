package steps;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Repayment 
{
WebDriver driver;
String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjo5LCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjEwMTgsImpvaW5zIjpbeyJmaWVsZHMiOiJhbGwiLCJzb3VyY2UtdGFibGUiOjk3MSwiY29uZGl0aW9uIjpbIj0iLFsiZmllbGQtaWQiLDEyMTA2XSxbImpvaW5lZC1maWVsZCIsIlN0YXRlbWVudCIsWyJmaWVsZC1pZCIsMTI1NzNdXV0sImFsaWFzIjoiU3RhdGVtZW50In0seyJmaWVsZHMiOiJhbGwiLCJzb3VyY2UtdGFibGUiOjEwNTMsImNvbmRpdGlvbiI6WyI9IixbImpvaW5lZC1maWVsZCIsIlN0YXRlbWVudCIsWyJmaWVsZC1pZCIsMTI1NjZdXSxbImpvaW5lZC1maWVsZCIsIkludmVzdG1lbnQiLFsiZmllbGQtaWQiLDExNzk1XV1dLCJhbGlhcyI6IkludmVzdG1lbnQifV0sImZpbHRlciI6WyJhbmQiLFsiPSIsWyJqb2luZWQtZmllbGQiLCJJbnZlc3RtZW50IixbImZpZWxkLWlkIiwxMTc5M11dLCIxIl0sWyJub3QtbnVsbCIsWyJmaWVsZC1pZCIsMTIxMDhdXSxbIj0iLFsiam9pbmVkLWZpZWxkIiwiU3RhdGVtZW50IixbImZpZWxkLWlkIiwxMjU3Nl1dLHRydWVdXSwiYWdncmVnYXRpb24iOltbImRpc3RpbmN0IixbImpvaW5lZC1maWVsZCIsIkludmVzdG1lbnQiLFsiZmllbGQtaWQiLDExNzg5XV1dXX0sInR5cGUiOiJxdWVyeSJ9LCJkaXNwbGF5Ijoic2NhbGFyIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319";
public Repayment(WebDriver driver)
{
	driver.get(Url);
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	String count=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
	System.out.println("Text is:"+count);
	try {
		
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
		Date date = new Date();
		String fileName = df.format(date);
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(file,
				new File("./InvestorRepaymnet/" + "Repayment" + " " + "(" + " " + fileName + " " + ")" + ".jpg"));
	} catch (Exception e) {
	}   

}
}
